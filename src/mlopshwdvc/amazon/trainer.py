import dvc.api
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC

params = dvc.api.params_show()
np.random.seed(params["random_state"])


def round_W(model, decimals=5):
    for i, j in model.__dict__.items():
        if isinstance(j, float):
            model.__dict__[i] = round(j, decimals)
        if isinstance(j, np.ndarray):
            model.__dict__[i] = np.around(j, decimals)


def train_model(df, columnsx, columnsy):
    df_train = df[df.split == "train"]
    if params["model_type"] == "sklearn_lr":
        model = LogisticRegression(**params["model_sklearn_lr_params"])
        model.fit(np.vstack(df_train[columnsx]), df_train[columnsy])
    elif params["model_type"] == "sklearn_svc":
        model = SVC(**params["model_sklearn_svc_params"])
        model.fit(np.vstack(df_train[columnsx]), df_train[columnsy])
    round_W(model)
    return model
