import dvc.api
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer

params = dvc.api.params_show()
np.random.seed(params["random_state"])


def train_vectorizer(df_input, columns):
    df_input = df_input[df_input.split == "train"]
    if params["vectorizer_type"] == "sklearn_tfidf":
        vec = TfidfVectorizer(**params["vectorizer_sklearn_tfidf_params"])
        vec.fit(df_input[columns].to_numpy())
        vec._stop_words_id = 0
    return vec


def vectorize(df_input, vec: TfidfVectorizer, columns):
    df = df_input
    if params["vectorizer_type"] == "sklearn_tfidf":
        array = vec.transform(df_input[columns].to_numpy()).toarray()
    df["_vecs"] = [row.tolist() for row in array.astype(float)]
    return df
