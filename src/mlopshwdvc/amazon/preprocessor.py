import re

import dvc.api
import pandas as pd
import numpy as np
import nltk

params = dvc.api.params_show()
np.random.seed(params["random_state"])


class Preprocessor:
    def __init__(self):
        from nltk.corpus import stopwords
        from nltk.stem import WordNetLemmatizer

        nltk.download("stopwords")
        nltk.download("wordnet")

        self.stop_words = set(stopwords.words("english"))
        self.url_pattern = re.compile(
            r"https?://\S+|www\.\S+|\[.*?\]|[^a-zA-Z\s]+|\w*\d\w*"
        )
        self.spec_chars_pattern = re.compile("[0-9 \-_]+")
        self.non_alpha_pattern = re.compile("[^a-z A-Z]+")
        self.lemmatizer = WordNetLemmatizer()

    def text_preprocessing(self, input_text: str) -> str:
        try:
            text = input_text.lower()
            text = self.url_pattern.sub("", text)
            text = self.spec_chars_pattern.sub(" ", text)
            text = self.non_alpha_pattern.sub(" ", text)
            text = " ".join(
                word for word in text.split() if word not in self.stop_words
            )
            text = text.strip().split(" ")
            text = [self.lemmatizer.lemmatize(token) for token in text]
            return " ".join(text)
        except:  # noqa
            return ""


def preprocess(df: pd.DataFrame, columns):
    if columns == "__all__":
        columns = df.select_dtypes(include=["object"]).columns.tolist()
    else:
        raise NotImplementedError()
    preprocesser = Preprocessor()
    for col in columns:
        df[str(col) + "_preprocessed"] = df[col].apply(
            preprocesser.text_preprocessing
        )
    preprocessed_columns = [str(col) + "_preprocessed" for col in columns]
    df["_corpus"] = df[preprocessed_columns].apply(
        lambda row: "_".join(row.values.astype(str)), axis=1
    )
    val, test = params["data_split"]["val"], params["data_split"]["test"]
    df["split"] = np.random.choice(
        ["train", "val", "test"], size=len(df), p=[1 - val - test, val, test]
    )
    return df
