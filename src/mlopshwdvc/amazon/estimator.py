import dvc.api
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.figure import Figure
from sklearn.metrics import (
    ConfusionMatrixDisplay,
    classification_report,
)

params = dvc.api.params_show()
np.random.seed(params["random_state"])


def conf_matrix(y_true: np.ndarray, pred: np.ndarray) -> Figure:
    plt.ioff()
    fig, ax = plt.subplots(figsize=(5, 5))
    ConfusionMatrixDisplay.from_predictions(
        y_true, pred, ax=ax, colorbar=False
    )
    ax.xaxis.set_tick_params(rotation=90)
    _ = ax.set_title("Confusion Matrix")
    plt.tight_layout()
    return fig


def test(df, model, columnsx, columnsy, output_figure):
    df = df[df.split == "test"]
    if params["model_type"] in ["sklearn_lr", "sklearn_svc"]:
        predicts = model.predict(np.vstack(df[columnsx]))
    conf_matrix(df[columnsy].astype(float), predicts)
    plt.savefig(output_figure)
    return classification_report(
        df[columnsy].astype(float), predicts, output_dict=True
    )
