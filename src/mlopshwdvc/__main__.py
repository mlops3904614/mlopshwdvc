import pickle
import json
import os
import pathlib

import click
import pandas as pd

import mlopshwdvc.amazon.preprocessor
import mlopshwdvc.amazon.vectorizer
import mlopshwdvc.amazon.trainer
import mlopshwdvc.amazon.estimator


@click.group
def cli():
    pass


@cli.command
@click.argument("input", type=pathlib.Path)
@click.argument("output", type=pathlib.Path)
@click.option("--columns", type=str, default="__all__")
@click.option("--limit", type=int, default=1000)
def preprocess(
    input: pathlib.Path, output: pathlib.Path, columns: str, limit: int
):
    df = {
        ".csv": lambda path: pd.read_csv(path, nrows=limit, header=None),
    }[input.suffix](input)

    df.columns = ["class", "title", "text"]
    df = df.head(limit)
    df = mlopshwdvc.amazon.preprocessor.preprocess(df, columns)

    os.makedirs(str(output.parent), exist_ok=True)
    {".parquet": lambda df, path: df.to_parquet(path, index=False)}[
        output.suffix
    ](df, output)


@cli.command
@click.argument("dataset", type=pathlib.Path)
@click.argument("output_model", type=pathlib.Path)
@click.option("--columns", type=str, default="_corpus")
def train_vectorizer(
    dataset: pathlib.Path, output_model: pathlib.Path, columns: str
):
    df = {".parquet": lambda path: pd.read_parquet(path)}[dataset.suffix](
        dataset
    )

    vec = mlopshwdvc.amazon.vectorizer.train_vectorizer(df, columns)

    os.makedirs(str(output_model.parent), exist_ok=True)
    {".pkl": lambda vec, path: pickle.dump(vec, path.open("wb"))}[
        output_model.suffix
    ](vec, output_model)


@cli.command
@click.argument("input", type=pathlib.Path)
@click.argument("model", type=pathlib.Path)
@click.argument("output_features", type=pathlib.Path)
@click.option("--columns", type=str, default="_corpus")
def vectorize(
    input: pathlib.Path,
    model: pathlib.Path,
    output_features: pathlib.Path,
    columns: str,
):
    df = {".parquet": lambda path: pd.read_parquet(path)}[input.suffix](input)
    vec = {".pkl": lambda path: pickle.load(path.open("rb"))}[model.suffix](
        model
    )

    df = mlopshwdvc.amazon.vectorizer.vectorize(df, vec, columns)

    os.makedirs(str(output_features.parent), exist_ok=True)
    {".parquet": lambda df, path: df.to_parquet(path, index=False)}[
        output_features.suffix
    ](df, output_features)


@cli.command()
@click.argument("dataset", type=pathlib.Path)
@click.argument("output_model", type=pathlib.Path)
@click.option("--columnsX", type=str, default="_vecs")
@click.option("--columnsY", type=str, default="class")
def train_model(
    dataset: pathlib.Path,
    output_model: pathlib.Path,
    columnsx: str,
    columnsy: str,
):
    df = {".parquet": lambda path: pd.read_parquet(path)}[dataset.suffix](
        dataset
    )

    model = mlopshwdvc.amazon.trainer.train_model(df, columnsx, columnsy)

    os.makedirs(str(output_model.parent), exist_ok=True)
    {".pkl": lambda model, path: pickle.dump(model, path.open("wb"))}[
        output_model.suffix
    ](model, output_model)


@cli.command()
@click.argument("dataset", type=pathlib.Path)
@click.argument("model", type=pathlib.Path)
@click.argument("output_metric", type=pathlib.Path)
@click.argument("output_figure", type=pathlib.Path)
@click.option("--columnsX", type=str, default="_vecs")
@click.option("--columnsY", type=str, default="class")
def test_model(
    dataset: pathlib.Path,
    model: pathlib.Path,
    output_metric: pathlib.Path,
    output_figure: pathlib.Path,
    columnsx: str,
    columnsy: str,
):
    df = {".parquet": lambda path: pd.read_parquet(path)}[dataset.suffix](
        dataset
    )
    model = {".pkl": lambda path: pickle.load(path.open("rb"))}[model.suffix](
        model
    )
    os.makedirs(str(output_figure.parent), exist_ok=True)
    metrics = mlopshwdvc.amazon.estimator.test(
        df, model, columnsx, columnsy, output_figure
    )
    os.makedirs(str(output_metric.parent), exist_ok=True)
    output_metric.write_text(json.dumps(metrics))


if __name__ == "__main__":
    cli()
