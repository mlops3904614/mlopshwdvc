FROM python:3.11-slim
WORKDIR /app
COPY requirements.dev.txt ./
RUN python -m pip install -r requirements.dev.txt --ignore-requires-python \
    --no-warn-script-location --no-warn-conflicts --no-build-isolation \
    --no-warn-conflicts --break-system-packages --ignore-installed --no-deps
CMD sh
