"""Generate the code reference pages and navigation."""

from pathlib import Path

import mkdocs_gen_files  # type: ignore # noqa

nav = mkdocs_gen_files.Nav()

root = Path(__file__).parent.parent
src = root / "notebooks"

for path in sorted(src.rglob("*.ipynb")):
    module_path = path.relative_to(src).with_suffix("")
    doc_path = path.relative_to(src).with_suffix(".md")
    full_doc_path = Path("notebooks", doc_path)

    parts = tuple(module_path.parts)
    nav[parts] = doc_path.as_posix()

    with mkdocs_gen_files.open(full_doc_path, "w") as fd:
        fd.write(f"::: {path}\n    handler: jupyter")

    mkdocs_gen_files.set_edit_path(full_doc_path, path.relative_to(root))

with mkdocs_gen_files.open("notebooks/SUMMARY.md", "w") as nav_file:
    nav_file.writelines(nav.build_literate_nav())
