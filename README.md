# mlopshwdvc

Be careful the api is not formed.

You can see how to take part in the project, how to use development tools and what methodology of the development process in the "CONTRIBUTING.md" file.

The project is distributed under the license specified in the file "LICENSE.txt".

You can use docker image with all requirements `docker build -f all_requirements.dockerfile -t "mlopshwdvc-all" .` and run, for example, python `docker run -it mlopshwdvc-all:latest python`
